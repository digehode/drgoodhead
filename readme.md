Dr Goodheaad
============

## About ##

Simple web server that logs and returns settings from headers, paths,
etc. to help debug various things that make requests to web servers.

## How it works ##

Extends the python BaseHTTPServer.HTTPServer and BaseHTTPServer.BaseHTTPRequestHandler

Implements a GET handler (more to come) that reports all useful information.

## Next steps ##

 1. Implement POST handler for equivalent stuff
 2. Possibly merge much of the GE/POST functionality to reduce repetition in the code
 3. Allow filtering of output that allows it to be still be useful
    (have date/time, not all headers, etc, but readable) unlike basic grepping leaves it
 4. Better hierarchichal output - later
