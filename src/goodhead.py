import BaseHTTPServer
import datetime
from urlparse import parse_qs

class GoodHead(BaseHTTPServer.HTTPServer):
    def __init__(self, server_address, requestHandlerClass):
        BaseHTTPServer.HTTPServer.__init__(self,server_address,requestHandlerClass)

class HeadHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self, ):
        """
        """
        date=datetime.datetime.now().isoformat()
        log="/------ START [%s]\n"%date
        log+=("| Request (%s) receieved from %s, port %d\n"%(self.command,self.client_address[0],self.client_address[1]))
        log+="| Path: %s\n| Ver: %s\n"%(self.path,self.request_version)
        log+="| /---- Headers ---\n"
        for i in self.headers.items():
            log+="| | "+i[0]+": "+i[1]+"\n"
        log+="| \--- /Headers ---\n"
        #print self.headers
        #print type(self.headers)
        #for i in dir(self.headers): print "\t",i
        #print self.headers.items()
        params=parse_qs(self.path[2:])
        log+="| /---- GET Parameters --- \n"
        for i in params:
            log+="| | %s:%s\n"%( i,str(params[i]))
        log+="| \--- /GET Parameters --- \n"
        #help(self.headers)
        log+="\---- END [%s]\n"%date
        print log
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(log)
        self.wfile.close()
if __name__ == '__main__':
    server_address = ('', 8000)
    httpd = GoodHead(server_address, HeadHandler)
    httpd.serve_forever()




